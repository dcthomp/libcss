/*
 * This file is part of LibCSS
 * Licensed under the MIT License,
 *		  http://www.opensource.org/licenses/mit-license.php
 * Copyright 2009 John-Mark Bell <jmb@netsurf-browser.org>
 */

#include "bytecode/bytecode.h"
#include "bytecode/opcodes.h"
#include "select/propset.h"
#include "select/propget.h"
#include "utils/utils.h"

#include "select/properties/properties.h"
#include "select/properties/helpers.h"

css_error css__cascade_representation(uint32_t opv, css_style *style,
		css_select_state *state)
{
	return css__cascade_uri_none(opv, style, state, set_representation);
}

css_error css__set_representation_from_hint(const css_hint *hint,
		css_computed_style *style)
{
	css_error error;

	error = set_representation(style, hint->status, hint->data.string);

	if (hint->data.string != NULL)
		lwc_string_unref(hint->data.string);

	return error;
}

css_error css__initial_representation(css_select_state *state)
{
	return set_representation(state->computed,
			CSS_REPRESENTATION_NONE, NULL);
}

css_error css__compose_representation(const css_computed_style *parent,
		const css_computed_style *child,
		css_computed_style *result)
{
	lwc_string *url;
	uint8_t type = get_representation(child, &url);

	if (type == CSS_REPRESENTATION_INHERIT) {
		type = get_representation(parent, &url);
	}

	return set_representation(result, type, url);
}

