/*
 * This file is part of LibCSS
 * Licensed under the MIT License,
 *		  http://www.opensource.org/licenses/mit-license.php
 * Copyright 2009 John-Mark Bell <jmb@netsurf-browser.org>
 */

#include "bytecode/bytecode.h"
#include "bytecode/opcodes.h"
#include "select/propset.h"
#include "select/propget.h"
#include "utils/utils.h"

#include "select/properties/properties.h"
#include "select/properties/helpers.h"

css_error css__cascade_color_by_array(uint32_t opv, css_style *style,
		css_select_state *state)
{
	return css__cascade_uri_none(opv, style, state, set_color_by_array);
}

css_error css__set_color_by_array_from_hint(const css_hint *hint,
		css_computed_style *style)
{
	css_error error;

	error = set_color_by_array(style, hint->status, hint->data.string);

	if (hint->data.string != NULL)
		lwc_string_unref(hint->data.string);

	return error;
}

css_error css__initial_color_by_array(css_select_state *state)
{
	return set_color_by_array(state->computed,
			CSS_COLOR_BY_ARRAY_NONE, NULL);
}

css_error css__compose_color_by_array(const css_computed_style *parent,
		const css_computed_style *child,
		css_computed_style *result)
{
	lwc_string *url;
	uint8_t type = get_color_by_array(child, &url);

	if (type == CSS_COLOR_BY_ARRAY_INHERIT) {
		type = get_color_by_array(parent, &url);
	}

	return set_color_by_array(result, type, url);
}

